import threading
import time

total = 4

def creates_item():
    global total
    for i in range(10):
        time.sleep(2)
        print('itaration {}'.format(i))
    print('itarations done')
    
def creates_items2():
    global total
    for i in range(10):
        time.sleep(1)
        print('itaration {}'.format(i))
        total += 1
    print('itarations done')

def limits_item():
    global total
    while True:
        if total > 5:
            print('overloaded')
            total -= 3
            print('subtracted by 3')
        else:
            time.sleep(1)
            print('wating')

creates1 = threading.Thread(target = creates_item)
creates2 = threading.Thread(target = creates_items2)
limiter = threading.Thread(target = limits_item, daemon = True)

creates1.start()
creates2.start()
limiter.start()

creates1.join()
creates2.join()
#limiter.join()

print('Finish with total {}'.format(total))

